package app;

import java.util.Optional;

interface Playeable {
    void walk(double x, double y);

    default void setGameName(String name) {
        System.out.println(name);
    }
}

interface Gameable {
    void startGame();

    default void setGameName(String name) {
        System.out.println(name);
    }
}

interface Soundable {
    void playMusic(String song);

    default void setGameName(String name) {
        System.out.println(name);
    }
}

public class App {
    private static String gameName = "Curso java";
    private static double playerPosX = 0;
    private static double playerPosY = 0;
    private static String songName = "Mi canción";

    public static void main(String... args) {

        Playeable play = (x, y) -> {
            playerPosX += x;
            playerPosY += y;
            System.out.println("playerPosX: " + playerPosX + ", playerPosY: " + playerPosY);
        };
        play.walk(1, 1);
        
        Gameable game = () -> System.out.println("gameName: " + gameName);
        game.setGameName("Mi juego");
        game.startGame();
        
        Soundable sound = (song) -> {
            Optional<String> cancion = Optional.ofNullable(song);
            songName = cancion.isPresent() ? song : "Solo canción";
            System.out.println("songName: " + songName);
        };
        sound.playMusic(songName);
    }

}