<html lang="en">
<jsp:include page="/header.jsp" flush="true">
    <jsp:param name="title" value="Ejemplo de jsp"/>
</jsp:include>

<body class="text-center">
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Ejemplo JSP</h1>
            <%
                out.write("<p class=\"lead font-weight-bold\">Parrafo impreso con Scriplets &lt;%= %&gt; </p>");
                out.write("<p class=\"lead\">Hola mundo</p>");
            %>
            <hr>
            <%= "<p class=\"lead font-weight-bold\">Parrafo impreso con Expressions &lt;% %&gt; </p>"%>
            <%= "<p class=\"lead\">Hola mundo </p>"%>
            <hr>
            <p class="lead font-weight-bold">Agrega alguno de los siguientes parametros al query de la ULR</p>
            <%
                String nombre = request.getParameter("Nombre");
                String apPaterno = request.getParameter("ApPaterno");
                String edad = request.getParameter("Edad");
            %>
            <%= "<p class=\"lead font-weight-bold\">Nombre: <small>" + (nombre != null ? nombre : "") + "</small></p>"%>
            <%= "<p class=\"lead font-weight-bold\">ApPaterno: <small>" + (apPaterno != null ? apPaterno : "") + "</small></p>"%>
            <%= "<p class=\"lead font-weight-bold\">Edad: <small>" + (edad != null ? edad : "") + "</small></p>"%>
        </div>
    </div>
</body>

</html>