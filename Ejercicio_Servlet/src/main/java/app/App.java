package app;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class App extends HttpServlet {
    
    private static final long serialVersionUID = 5430513214412853815L;

    private static final String CSSCOVER = "<style>/* * Globals */ /* Links */ a, a:focus, a:hover { color: #fff; } /* Custom default button */ .btn-secondary, .btn-secondary:hover, .btn-secondary:focus { color: #333; text-shadow: none; /* Prevent inheritance from `body` */ background-color: #fff; border: .05rem solid #fff; } /* * Base structure */ html, body { height: 100%; background-color: #333; } body { display: -ms-flexbox; display: flex; color: #fff; text-shadow: 0 .05rem .1rem rgba(0, 0, 0, .5); box-shadow: inset 0 0 5rem rgba(0, 0, 0, .5); } .cover-container { max-width: 42em; } /* * Header */ .masthead { margin-bottom: 2rem; } .masthead-brand { margin-bottom: 0; } .nav-masthead .nav-link { padding: .25rem 0; font-weight: 700; color: rgba(255, 255, 255, .5); background-color: transparent; border-bottom: .25rem solid transparent; } .nav-masthead .nav-link:hover, .nav-masthead .nav-link:focus { border-bottom-color: rgba(255, 255, 255, .25); } .nav-masthead .nav-link + .nav-link { margin-left: 1rem; } .nav-masthead .active { color: #fff; border-bottom-color: #fff; } @media (min-width: 48em) { .masthead-brand { float: left; } .nav-masthead { float: right; } } /* * Cover */ .cover { padding: 0 1.5rem; } .cover .btn-lg { padding: .75rem 1.25rem; font-weight: 700; } /* * Footer */ .mastfoot { color: rgba(255, 255, 255, .5); }</style>";
    private static final String HTML_HEAD = "<!doctype html><html><head><base href=\"/\" target=\"_blank\"><title>Hello, world!</title>"+
    "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">"+
    "<script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>"+
    "<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>"+
    "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\" integrity=\"sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6\" crossorigin=\"anonymous\"></script>"+
    CSSCOVER+
    "</head>";

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) {
        res.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter pw = res.getWriter()) {
            pw.println(HTML_HEAD);
            pw.println("<body class=\"text-center\">"+
                "<div class=\"cover-container d-flex w-100 h-100 p-3 mx-auto flex-column\">"+
                    "<header class=\"masthead mb-auto\"></header>"+
                    "<main role=\"main\" class=\"inner cover\">"+
                        "<h1 class=\"cover-heading\">Página Cover.</h1>"+
                        "<p class=\"lead\">Cover del ejercicio de servlets.</p>"+
                        "<p class=\"lead\">"+
                            "<a href=\"/appWar/login.jsp\" target=\"_self\" class=\"btn btn-lg btn-secondary\">Ir al login more</a>"+
                        "</p>"+
                    "</main>"+
                    "<footer class=\"mastfoot mt-auto\"></footer>"+
                "</div>"+
            "</body>");
        pw.println("</html>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        res.setStatus(200);
        return;
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) {
        String user = req.getParameter("username");
        String password = req.getParameter("password");
        res.setContentType("application/json;charset=UTF-8");
        try (PrintWriter pw = res.getWriter()) {
            pw.println("{ \"user\": \"" + user + "\", \"password\": \"" + password + "\"}");
        } catch (IOException e) {
            e.printStackTrace();
        }
        res.setStatus(200);
        return;
    }
}
