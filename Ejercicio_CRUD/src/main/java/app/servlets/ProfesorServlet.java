package app.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.beans.Profesor;
import app.services.ProfesorService;

public class ProfesorServlet extends HttpServlet {

    private static final long serialVersionUID = 6456707013143383689L;
    private static final ProfesorService servicio = new ProfesorService();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Profesor> Profesors = servicio.getProfesores();
            Profesors.forEach(Profesor -> {
                System.out.println(Profesor);
            });
            getServletContext().getRequestDispatcher("/WEB-INF/privates/profesoresList.jsp").forward(request, response);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }
    
}