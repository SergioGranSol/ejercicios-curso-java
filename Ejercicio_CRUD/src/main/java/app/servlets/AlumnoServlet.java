package app.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.beans.Alumno;
import app.services.AlumnoService;

public class AlumnoServlet extends HttpServlet {

    private static final long serialVersionUID = 1334904544042391196L;
    private static final AlumnoService servicio = new AlumnoService();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Alumno> Alumnos = servicio.getAlumnos();
            Alumnos.forEach(Alumno -> {
                System.out.println(Alumno);
            });
            getServletContext().getRequestDispatcher("/WEB-INF/privates/alumnosList.jsp").forward(request, response);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }        
    }
    
}