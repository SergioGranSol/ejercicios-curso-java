package app.services;

import java.sql.SQLException;
import java.util.List;

import app.beans.Alumno;
import app.repositories.AlumnoRepository;

public class AlumnoService {

    private AlumnoRepository AlumnoRepositorio = AlumnoRepository.newInstance(true);

    public List<Alumno> getAlumnos() throws SQLException {
        List<Alumno> Alumnos = AlumnoRepositorio.findAll();
        Alumnos.forEach(Alumno -> {
            System.out.println(Alumno.toString());
        });
        return Alumnos;
    }
}