package app.services;

import java.sql.SQLException;
import java.util.List;

import app.beans.Profesor;
import app.repositories.ProfesorRepository;

public class ProfesorService {

    private ProfesorRepository ProfesorRepositorio = ProfesorRepository.newInstance(true);

    public List<Profesor> getProfesores() throws SQLException {
        List<Profesor> Profesores = ProfesorRepositorio.findAll();
        Profesores.forEach(Profesor -> {
            System.out.println(Profesor.toString());
        });
        return Profesores;
    }
}