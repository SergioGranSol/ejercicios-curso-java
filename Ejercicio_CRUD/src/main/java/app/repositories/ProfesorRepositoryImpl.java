package app.repositories;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import app.beans.Profesor;

public class ProfesorRepositoryImpl implements ProfesorRepository {

    private static final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("profesor");

    @Override
    public List<Profesor> findAll() throws SQLException {
        EntityManager em = emFactory.createEntityManager();
        TypedQuery<Profesor> query = em.createNamedQuery("profesor.findAll", Profesor.class);
        List<Profesor> Profesors = query.getResultList();
        em.close();
        return Profesors;
    }

    @Override
    public Integer insert(Profesor Profesor) throws SQLException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer update(Profesor Profesor) throws SQLException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer delete(Long id) throws SQLException {
        // TODO Auto-generated method stub
        return null;
    };
    
}