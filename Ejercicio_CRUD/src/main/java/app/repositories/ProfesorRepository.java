package app.repositories;

import java.sql.SQLException;
import java.util.List;

import app.beans.Profesor;


public interface ProfesorRepository {
    List<Profesor> findAll() throws SQLException;
    Integer insert(Profesor Profesor) throws SQLException;
    Integer update(Profesor Profesor) throws SQLException;
    Integer delete(Long id) throws SQLException;

    static ProfesorRepository newInstance(Boolean type){
        return new ProfesorRepositoryImpl();
    }
}