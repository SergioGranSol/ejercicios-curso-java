package app.repositories;

import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import app.beans.Alumno;

public class AlumnoRepositoryImpl implements AlumnoRepository {

    private static final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("DBUnit");

    @Override
    public List<Alumno> findAll() throws SQLException {
        EntityManager em = emFactory.createEntityManager();
        TypedQuery<Alumno> query = em.createNamedQuery("alumno.findAll", Alumno.class);
        List<Alumno> alumnos = query.getResultList();
        em.close();
        return alumnos;
    }

    @Override
    public Integer insert(Alumno Alumno) throws SQLException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer update(Alumno Alumno) throws SQLException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer delete(Long id) throws SQLException {
        // TODO Auto-generated method stub
        return null;
    }
}