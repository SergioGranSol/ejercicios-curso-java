package app.repositories;

import java.sql.SQLException;
import java.util.List;

import app.beans.Alumno;


public interface AlumnoRepository {
    List<Alumno> findAll() throws SQLException;
    Integer insert(Alumno Alumno) throws SQLException;
    Integer update(Alumno Alumno) throws SQLException;
    Integer delete(Long id) throws SQLException;

    static AlumnoRepository newInstance(Boolean type){
        return new AlumnoRepositoryImpl();
    }
}