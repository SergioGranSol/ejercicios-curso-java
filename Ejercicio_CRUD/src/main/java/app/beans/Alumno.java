package app.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name="alumno")
@Table(name="alumnos")
@NamedQuery(name="alumno.findAll", query="SELECT a FROM alumno a") 
public class Alumno implements Serializable{

    private static final long serialVersionUID = -6410748150339889368L;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id = 0L;

    @Column(length=100)
    private String nombre = "";

    @Column(length=100)
    private String apellido = "";

    private Integer edad = 0;

    @Column(length=500)
    private String escuela = "";

    public Alumno(Long id, String nombre, String apellido, Integer edad, String escuela) {
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getEscuela() {
        return escuela;
    }

    public void setEscuela(String escuela) {
        this.escuela = escuela;
    }
    
    public Alumno withId(Long id) {
        this.id = id;
        return this;
    }

    public Alumno withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public Alumno withApellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public Alumno withEdad(Integer edad) {
        this.edad = edad;
        return this;
    }

    public Alumno withEscuela(String escuela) {
        this.escuela = escuela;
        return this;
    }

    @Override
    public String toString() {
        return "Alumno [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", escuela="
                + escuela + "]";
    }    

}