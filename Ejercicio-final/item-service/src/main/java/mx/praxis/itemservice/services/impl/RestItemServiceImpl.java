package mx.praxis.itemservice.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import mx.praxis.itemservice.dtos.Item;
import mx.praxis.itemservice.dtos.Product;
import mx.praxis.itemservice.services.ItemService;

@Service
public class RestItemServiceImpl implements ItemService {

    private final RestTemplate restTemplate;

    public RestItemServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Item> findAllItems() {
        ResponseEntity<List<Product>> products = restTemplate
            .exchange("http://product-service/",
            HttpMethod.GET, null,
            new ParameterizedTypeReference<List<Product>>() {});
        List<Item> items = products.getBody()
            .stream()
            .map(p -> new Item(p, 1))
            .collect(Collectors.toList());
        return items;
    }

    @Override
    public Item findItemById(Long id, Integer amount) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("id", id.toString());
        ResponseEntity<Product> product = restTemplate
            .exchange("http://product-service/{id}",
            HttpMethod.GET, null,
            new ParameterizedTypeReference<Product>() {}, parameters);
        return new Item(product.getBody(), amount);
    }
}