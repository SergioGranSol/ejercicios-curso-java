package mx.praxis.itemservice.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.praxis.itemservice.dtos.Item;
import mx.praxis.itemservice.dtos.Product;
import mx.praxis.itemservice.feing.client.ProductFeingClient;
import mx.praxis.itemservice.services.ItemService;

@Service()
public class FeingItemServiceImpl implements ItemService {

    private ProductFeingClient productFeingClient;

    public FeingItemServiceImpl(ProductFeingClient productFeingClient) {
        this.productFeingClient = productFeingClient;
    }

    @Override
    public List<Item> findAllItems() {
        return productFeingClient.getAllProducts()
            .getBody()
            .stream()
            .map(p -> new Item(p, 1))
            .collect(Collectors.toList());
    }

    @Override
	public Item findItemById(Long id, Integer amount) {
        ResponseEntity<Product> product = productFeingClient.getProductById(id);
        return new Item(product.getBody(), amount);
    }
}