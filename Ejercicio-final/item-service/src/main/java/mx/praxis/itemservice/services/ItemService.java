package mx.praxis.itemservice.services;

import java.util.List;

import mx.praxis.itemservice.dtos.Item;

public interface ItemService {
    
    List<Item> findAllItems();

    Item findItemById(Long id, Integer amount);
}