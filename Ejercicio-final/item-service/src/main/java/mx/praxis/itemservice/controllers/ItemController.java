package mx.praxis.itemservice.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.praxis.itemservice.dtos.Item;
import mx.praxis.itemservice.services.ItemService;

@RestController
@RequestMapping("/")
public class ItemController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);
    private final ItemService itemService;

    public ItemController(@Qualifier("feingItemServiceImpl") ItemService itemService) {
        this.itemService = itemService;
    }
    
    @GetMapping(value="/all", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Item>> getItems() {
        LOGGER.info("getItems controller starts");
        List<Item> items = itemService.findAllItems();
        LOGGER.info("getItems controller ends");
        return ResponseEntity.ok(items);
    }

    @GetMapping(value="/{id}/{amount}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Item> getItemById(@PathVariable Long id, @PathVariable Integer amount) {
        LOGGER.info("getItemById controller starts");
        Item item = itemService.findItemById(id, amount);
        LOGGER.info("getItemById controller ends");
        return ResponseEntity.ok(item);
    }

    @GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Item> getItemById(@PathVariable Long id) {
        LOGGER.info("getItemById controller starts");
        Item item = itemService.findItemById(id, 1);
        LOGGER.info("getItemById controller ends");
        return ResponseEntity.ok(item);
    }
}