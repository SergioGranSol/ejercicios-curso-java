package mx.praxis.itemservice.feing.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import mx.praxis.itemservice.dtos.Product;

@FeignClient(name="product-service")
public interface ProductFeingClient {

    @GetMapping(value="/all", produces=MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<Product>> getAllProducts();

    @GetMapping(value="/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Product> getProductById(@PathVariable Long id);
}