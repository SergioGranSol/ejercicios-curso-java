package mx.praxis.invoiceservice.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import mx.praxis.invoiceservice.dtos.Invoice;

public interface InvoiceRepository extends PagingAndSortingRepository<Invoice, Integer> {
 
    List<Invoice> findAll();

}