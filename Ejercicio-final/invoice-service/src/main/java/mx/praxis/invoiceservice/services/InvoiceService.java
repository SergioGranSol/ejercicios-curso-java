package mx.praxis.invoiceservice.services;

import java.util.List;

import mx.praxis.invoiceservice.dtos.Invoice;

public interface InvoiceService {
    
    List<Invoice> findAllInvoices();

    Invoice saveInvoice(Invoice invoice);
}