package mx.praxis.invoiceservice.dtos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Getter
@Setter
@Entity
@Table(name = "invoice")
public class Invoice implements Serializable {

    private static final long serialVersionUID = -4927538507485662629L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    public Invoice id(Long id) {
        this.id = id;
        return this;
    }

    @ManyToMany
    private List<Item> items;
    public Invoice items(List<Item> items) {
        this.items = items;
        return this;
    }
    
    @Column(name="items_list", nullable=false)
    private String itemsList;
    public Invoice itemsList(String itemsList) {
        this.itemsList = itemsList;
        return this;
    }

    private Boolean paid;
    public Invoice paid(Boolean paid) {
        this.paid = paid;
        return this;
    }
        
    @Column(name="create_at", nullable=false)
    @Temporal(TemporalType.DATE)
    private Date createAt;
    public Invoice createAt(Date createAt) {
        this.createAt = createAt;
        return this;
    }

    public Double getTotal(){
        Double total = 0d;
        for(int i = 0; i < this.items.size(); i++){
            total += this.items.get(i).getTotal() * this.items.get(i).getAmount();
        }
        return total;
    }
}