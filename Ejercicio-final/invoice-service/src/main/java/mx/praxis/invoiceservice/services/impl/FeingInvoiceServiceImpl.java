package mx.praxis.invoiceservice.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.praxis.invoiceservice.dtos.Invoice;
import mx.praxis.invoiceservice.dtos.Item;
import mx.praxis.invoiceservice.feing.client.ItemFeingClient;
import mx.praxis.invoiceservice.repositories.InvoiceRepository;
import mx.praxis.invoiceservice.services.InvoiceService;

@Service()
public class FeingInvoiceServiceImpl implements InvoiceService {

    private ItemFeingClient itemFeingClient;
    private final InvoiceRepository invoiceRepository;

    public FeingInvoiceServiceImpl(ItemFeingClient itemFeingClient, InvoiceRepository invoiceRepository) {
        this.itemFeingClient = itemFeingClient;
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> findAllInvoices() {
        List<Invoice> invoices = invoiceRepository.findAll();
        for (int i = 0; i < invoices.size(); i++) {
            List<String> itemsList = new ArrayList<String>(Arrays.asList(invoices.get(i).getItemsList().split(",")));
            List<Item> items = new ArrayList<Item>();
            for (int j = 0; j < itemsList.size(); j++){
                Long itemId = new Long(Arrays.asList(itemsList.get(j).split(":")).get(0));
                Integer itemAmount = new Integer(Arrays.asList(itemsList.get(j).split(":")).get(1));
                ResponseEntity<Item> item = itemFeingClient.getItemById(itemId, itemAmount);
                items.add(item.getBody());
            }
            invoices.get(i).setItems(items);
        }
        return invoices;
    }

    @Override
	public Invoice saveInvoice(Invoice invoice) {
        invoiceRepository.save(invoice);
        List<String> itemsList = new ArrayList<String>(Arrays.asList(invoice.getItemsList().split(",")));
        List<Item> items = new ArrayList<Item>();
        for (int j = 0; j < itemsList.size(); j++){
            Long itemId = new Long(Arrays.asList(itemsList.get(j).split(":")).get(0));
            Integer itemAmount = new Integer(Arrays.asList(itemsList.get(j).split(":")).get(1));
            ResponseEntity<Item> item = itemFeingClient.getItemById(itemId, itemAmount);
            items.add(item.getBody());
        }
        invoice.setItems(items);
        return invoice;
    }
}