package mx.praxis.invoiceservice.dtos;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Builder
@Getter
@Setter
@Entity
@Table(name = "Items")
public class Item implements Serializable {

    private static final long serialVersionUID = -4862075524662759938L;

    @Id
    private Product product;

    private Integer amount;

    public Double getTotal(){
        return product.getPrice() * this.amount;
    }
}