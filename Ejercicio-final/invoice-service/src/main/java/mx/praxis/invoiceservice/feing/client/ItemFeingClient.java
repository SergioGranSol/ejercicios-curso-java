package mx.praxis.invoiceservice.feing.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import mx.praxis.invoiceservice.dtos.Item;

@FeignClient(name="item-service")
public interface ItemFeingClient {

    @GetMapping(value="/", produces=MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<Item>> getAllItems();

    @GetMapping(value="/{id}/{amount}", produces=MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Item> getItemById(@PathVariable Long id, @PathVariable Integer amount);

}