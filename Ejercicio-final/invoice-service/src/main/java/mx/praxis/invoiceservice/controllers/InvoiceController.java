package mx.praxis.invoiceservice.controllers;

import java.sql.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.praxis.invoiceservice.dtos.Invoice;
import mx.praxis.invoiceservice.services.InvoiceService;

@RestController
@RequestMapping("/")
public class InvoiceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceController.class);
    private final InvoiceService invoiceService;

    public InvoiceController(@Qualifier("feingInvoiceServiceImpl") InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    
    @GetMapping(value="/all", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Invoice>> getInvoices() {
        LOGGER.info("getInvoices controller starts");
        List<Invoice> invoices = invoiceService.findAllInvoices();
        LOGGER.info("getInvoices controller ends");
        return ResponseEntity.ok(invoices);
    }

    @PostMapping(value="/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Invoice> createInvoice(@RequestBody MultiValueMap<String, String> formData) {
        LOGGER.info("createInvoice controller starts");
        Invoice invoice = new Invoice()
            .itemsList(formData.get("itemList").get(0))
            .paid(new Boolean(formData.get("paid").get(0)))
            .createAt(new Date(new Long(System.currentTimeMillis())));   
        invoiceService.saveInvoice(invoice);
        LOGGER.info("createInvoice controller ends");
        return ResponseEntity.ok(invoice);
    }
}