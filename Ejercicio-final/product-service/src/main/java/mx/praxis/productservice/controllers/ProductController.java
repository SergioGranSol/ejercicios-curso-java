package mx.praxis.productservice.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.praxis.productservice.beans.Product;
import mx.praxis.productservice.services.ProductService;

@RestController
@RequestMapping("/")
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
    private ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }
    
    @GetMapping(value="/all")
    public ResponseEntity<List<Product>> getProducts() {
        LOGGER.info("getProducts controller starts");
        List<Product> products = productService.findAllProducts();
        LOGGER.info("getProducts controller ends");
        return ResponseEntity.ok(products);
    }

    @GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Product> getProductById(@PathVariable Long id) {
        LOGGER.info("getProductById controller starts");
        Product product = productService.findProducById(id);
        LOGGER.info("getProductById controller ends");
        return ResponseEntity.ok(product);
    }
}