package mx.praxis.productservice.services;

import java.util.List;

import mx.praxis.productservice.beans.Product;

public interface ProductService {
    
    List<Product> findAllProducts();

    Product findProducById(Long id);
}