package mx.praxis.productservice.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import mx.praxis.productservice.beans.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {
 
    List<Product> findAll();

    Product findProductById(Long id);
}