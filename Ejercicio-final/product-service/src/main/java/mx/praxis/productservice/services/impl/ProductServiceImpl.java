package mx.praxis.productservice.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import mx.praxis.productservice.beans.Product;
import mx.praxis.productservice.repositories.ProductRepository;
import mx.praxis.productservice.services.ProductService;

@Service(value = "productServiceImpl")
public class ProductServiceImpl implements ProductService {

    private final ProductRepository producRepository;

    public ProductServiceImpl(ProductRepository producRepository) {
        this.producRepository = producRepository;
    }

    @Override
    public List<Product> findAllProducts() {
        return producRepository.findAll();
    }
    
    @Override
    public Product findProducById(Long id) {
        return producRepository.findProductById(id);
    }
}