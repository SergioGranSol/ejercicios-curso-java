package app.threads;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ThreadExample {

    private static final Logger logger = LogManager.getLogger();
    
    public void testThread() {

        Thread t1 = new Thread(()-> logger.trace("Test trace"), "Thread 1");
        Thread t2 = new Thread(()-> logger.debug("Test debug"), "Thread 2");
        Thread t3 = new Thread(()-> logger.info("Test info"), "Thread 3");
        Thread t4 = new Thread(()-> logger.warn("Test warn"), "Thread 4");
        Thread t5 = new Thread(()-> logger.error("Test error"), "Thread 5");
        Thread t6 = new Thread(()-> logger.fatal("Test fatal"), "Thread 6");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
    }

}