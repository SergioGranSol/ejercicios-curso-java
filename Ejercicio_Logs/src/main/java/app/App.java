package app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.threads.ThreadExample;

public class App {

	private static final Logger logger = LogManager.getLogger();
	private static ThreadExample threads = new ThreadExample();

	public static void main(String[] args) {
		
		logger.trace("Test Trace");
		logger.debug("Test Debug");
		logger.info("Test Info");
		logger.warn("Test Warn");
		logger.error("Test Error");
		logger.fatal("Test Fatal");

		threads.testThread();
	}
}