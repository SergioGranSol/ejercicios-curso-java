package app.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.beans.Profesor;
import app.services.ProfesorServicesImpl;

public class ProfesorServlet extends HttpServlet {

    private static final long serialVersionUID = 468929142820525723L;
    private static final ProfesorServicesImpl servicio = new ProfesorServicesImpl();
    private static final String bootstrapFramework = "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">"+
    "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\" integrity=\"sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6\" crossorigin=\"anonymous\"></script>"+
    "<script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>"+
    "<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>";

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        List<Profesor> profesores = servicio.selectProfesores();

        response.setContentType("text/html");
        out.println("<!doctype html>");
        out.println("<html>");
        out.println("<head><title>Hello, world!</title>"+bootstrapFramework+"</head>");
        out.println("<body>");
        out.println("<div class=\"jumbotron pb-2\">"+
            "<h1 class=\"display-4\">Hello, world!</h1>"+
                "<p class=\"lead\">Este es mi primer servlet.</p>"+
                "<hr class=\"my-4\">"+
                "<p>Lista de registros en base de datos.</p>"+
            "</div>");
        out.println("<div class=\"m-3\"><ul class=\"list-group\">");
        out.println("<li class=\"list-group-item active py-1\">Lista</li>");
        profesores.forEach(profesor -> {
            out.println("<li class=\"list-group-item py-1\">"+profesor+"</li>");
        });
        out.println("</ul></div>");
        out.println("</body></html>");
    }
    
}