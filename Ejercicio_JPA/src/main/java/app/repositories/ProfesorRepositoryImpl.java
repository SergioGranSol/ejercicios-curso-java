package app.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import app.beans.Profesor;

public class ProfesorRepositoryImpl implements ProfesorRepository {

    private static final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("profesor");

    public List<Profesor> selectAll() {
        EntityManager em = emFactory.createEntityManager();
        TypedQuery<Profesor> query = em.createNamedQuery("profesor.findAll", Profesor.class);
        List<Profesor> profesores = query.getResultList();
        em.close();
        return profesores;
    };
    
}