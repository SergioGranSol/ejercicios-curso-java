package app.repositories;

import java.util.List;

import app.beans.Profesor;

public interface ProfesorRepository {
    List<Profesor> selectAll();
}