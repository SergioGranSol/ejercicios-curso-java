package app.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name="profesor")
@Table(name="profesores")
@NamedQuery(name="profesor.findAll", query="SELECT p FROM profesor p") 
public class Profesor implements Serializable{

    private static final long serialVersionUID = 987654345678764326L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id = 0L;

    @Column(length=100)
    private String nombre = "";

    @Column(length=100)
    private String apellido = "";

    private Integer edad = 0;

    @Column(length=500)
    private String materia = "";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }
    
    public Profesor withId(Long id) {
        this.id = id;
        return this;
    }

    public Profesor withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public Profesor withApellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public Profesor withEdad(Integer edad) {
        this.edad = edad;
        return this;
    }

    public Profesor withMateria(String materia) {
        this.materia = materia;
        return this;
    }

    @Override
    public String toString() {
        return "Profesor [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", materia="
                + materia + "]";
    }    

}