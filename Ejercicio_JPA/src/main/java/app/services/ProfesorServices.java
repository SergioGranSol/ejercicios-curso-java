package app.services;

import java.util.List;

import app.beans.Profesor;

public interface ProfesorServices {
    List<Profesor> selectProfesores();
}