package app.services;

import java.util.List;

import app.beans.Profesor;
import app.repositories.ProfesorRepository;
import app.repositories.ProfesorRepositoryImpl;

public class ProfesorServicesImpl implements ProfesorServices {

    private ProfesorRepository profesorRepositorio = new ProfesorRepositoryImpl();

    public List<Profesor> selectProfesores() {
        List<Profesor> profesores = profesorRepositorio.selectAll();
        profesores.forEach(profesor -> {
            System.out.println(profesor.toString());
        });
        return profesores;
    }
}