package app.repositories;

import java.sql.SQLException;
import java.util.List;

import app.beans.Estudiante;

public interface EstudianteRepository {
    List<Estudiante> selectAll() throws SQLException;
    Integer insert(Estudiante estudiante) throws SQLException;
    Integer insertAll(List<Estudiante> estudiantes) throws SQLException;
    Integer update(Estudiante estudiante) throws SQLException;
    Integer updateMany(List<Estudiante> estudiantes) throws SQLException;
    Integer delete(Long id) throws SQLException;
    Integer deleteMany(List<Long> ids) throws SQLException;

    static EstudianteRepository newInstance(String type){
        switch (type) {
            case "A":
                return new EstudianteRepositoryImplA();
            case "B":
                return new EstudianteRepositoryImplB();
            case "C":
                return new EstudianteRepositoryImplC();
            default:
                break;
        }
        return new EstudianteRepositoryImplA();
    }
}