package app.repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import app.beans.Estudiante;

/*
 * Implementacion sin transacciones
 */

public class EstudianteRepositoryImplA implements EstudianteRepository {

    private static String url = "jdbc:postgresql://localhost:5432/cursoJava?user=postgres&password=postgres";

    public EstudianteRepositoryImplA() {
        System.out.println("Implementacion A: Implementacion sin transacciones");
    }
    
    public List<Estudiante> selectAll() throws SQLException {
        String SQL = "select * from estudiantes order by id";
        List<Estudiante> estudiantes = new ArrayList<Estudiante>();
        try (Connection conn = DriverManager.getConnection(url)){
            PreparedStatement ps = conn.prepareStatement(SQL);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    Estudiante estudiante = new Estudiante()
                        .withId(rs.getLong("id"))
                        .withNombre(rs.getString("nombre"))
                        .withApellido(rs.getString("apellido"))
                        .withEdad(rs.getInt("edad"))
                        .withEscuela(rs.getString("escuela"));
                    estudiantes.add(estudiante);
                }
            }
        }
        return estudiantes;
    };
    
    public Integer insert(Estudiante estudiante) throws SQLException {
        Integer count = 0;
        String SQL = "insert into estudiantes(";
        String SQLvalues = " values(";
        if(estudiante.getNombre().length() > 0){
            SQL += "nombre,";
            SQLvalues += "?,";
        }
        if(estudiante.getApellido().length() > 0){
            SQL += "apellido,";
            SQLvalues += "?,";
        }
        if(estudiante.getEdad() > 0){
            SQL += "edad,";
            SQLvalues += "?,";
        }
        if(estudiante.getEscuela().length() > 0){
            SQL += "escuela,";
            SQLvalues += "?,";
        }
        SQL = SQL.substring(0, SQL.length() - 1) + ")" + SQLvalues.substring(0, SQLvalues.length() - 1) + ")";
        System.out.println("SQL insert: " + SQL);
        try (Connection conn = DriverManager.getConnection(url)){
            try(PreparedStatement ps = conn.prepareStatement(SQL)){
                if(estudiante.getNombre().length() > 0) ps.setString(++count, estudiante.getNombre());
                if(estudiante.getApellido().length() > 0) ps.setString(++count, estudiante.getApellido());
                if(estudiante.getEdad() > 0) ps.setInt(++count, estudiante.getEdad());
                if(estudiante.getEscuela().length() > 0) ps.setString(++count, estudiante.getEscuela());
                Integer result = count == 0 ? -1 : ps.executeUpdate();
                return result;
            } catch (SQLException e) {
                System.out.println(e);
                conn.rollback();
                throw e;
            }
        }
    }

    public Integer insertAll(List<Estudiante> estudiantes) throws SQLException {
        Integer insertados = 0;
        Integer registro = 0;
        for (Estudiante estudiante : estudiantes){
            registro = this.insert(estudiante);
            insertados = registro != -1 ? (insertados + registro) : insertados;
        }
        return insertados;
    }
    
    public Integer update(Estudiante estudiante) throws SQLException {
        Integer count = 0;
        String SQL = "update estudiantes set ";
        if(estudiante.getNombre().length() > 0) SQL += "nombre = ?,";
        if(estudiante.getApellido().length() > 0) SQL += "apellido = ?,";
        if(estudiante.getEdad() > 0) SQL += "edad = ?,";
        if(estudiante.getEscuela().length() > 0) SQL += "escuela = ?,";
        SQL = SQL.substring(0, SQL.length() - 1) + " where id = ?";
        System.out.println("SQL update: " + SQL);
        try (Connection conn = DriverManager.getConnection(url)){
            try (PreparedStatement ps = conn.prepareStatement(SQL)){
                if(estudiante.getNombre().length() > 0) ps.setString(++count, estudiante.getNombre());
                if(estudiante.getApellido().length() > 0) ps.setString(++count, estudiante.getApellido());
                if(estudiante.getEdad() > 0) ps.setInt(++count, estudiante.getEdad());
                if(estudiante.getEscuela().length() > 0) ps.setString(++count, estudiante.getEscuela());
                ps.setLong(++count, estudiante.getId());
                Integer result = count == 1 ? -1 : ps.executeUpdate();
                return result;
            } catch (SQLException e) {
                System.out.println(e);
                conn.rollback();
                throw e;
            }
        }
    }

    public Integer updateMany(List<Estudiante> estudiantes) throws SQLException {
        Integer actualizados = 0;
        Integer registro = 0;
        for (Estudiante estudiante : estudiantes){
            registro = this.update(estudiante);
            actualizados = registro != -1 ? (actualizados + registro) : actualizados;
        }
        return actualizados;
    }

    public Integer delete(Long id) throws SQLException {
        String SQL = "delete from estudiantes where id = ?";
        System.out.println("SQL delete: " + SQL);
        try (Connection conn = DriverManager.getConnection(url)){
            try(PreparedStatement ps = conn.prepareStatement(SQL)){
                ps.setLong(1, id);
                Integer eliminado = ps.executeUpdate();
                return eliminado >= 0 ? eliminado : -1;
            } catch (SQLException e) {
                System.out.println(e);
                conn.rollback();
                throw e;
            }
        }
    }

    public Integer deleteMany(List<Long> ids) throws SQLException {
        Integer eliminados = 0;
        Integer registro = 0;
        for (Long id : ids){
            registro = this.delete(id);
            eliminados = registro != -1 ? (eliminados + registro) : eliminados;
        }
        return eliminados;
    }
    
}