package app;

import app.services.EstudianteServicesImpl;

public class App {
    public static void main(String[] args) throws Exception {
        EstudianteServicesImpl estudianteServicio = new EstudianteServicesImpl();
        estudianteServicio.selectEstudiantes();
        estudianteServicio.insertEstudiante();
        estudianteServicio.insertEstudiantes();
        estudianteServicio.updateEstudiante();
        estudianteServicio.updateEstudiantes();
        estudianteServicio.deleteEstudiante();
        estudianteServicio.deleteEstudiantes();
    }
}