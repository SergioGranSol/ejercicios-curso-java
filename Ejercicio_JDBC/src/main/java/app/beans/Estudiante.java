package app.beans;

import java.io.Serializable;

public class Estudiante implements Serializable {
    
    private static final long serialVersionUID = 234567876543234567L;

    private Long id = 0L;
    private String nombre = "";
    private String apellido = "";
    private Integer edad = 0;
    private String escuela = "";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getEscuela() {
        return escuela;
    }

    public void setEscuela(String escuela) {
        this.escuela = escuela;
    }

    public Estudiante withId(Long id) {
        this.id = id;
        return this;
    }

    public Estudiante withNombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public Estudiante withApellido(String apellido) {
        this.apellido = apellido;
        return this;
    }

    public Estudiante withEdad(Integer edad) {
        this.edad = edad;
        return this;
    }

    public Estudiante withEscuela(String escuela) {
        this.escuela = escuela;
        return this;
    }

    @Override
    public String toString() {
        return "Estudiante [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad
                + ", escuela=" + escuela + "]";
    }
    
}