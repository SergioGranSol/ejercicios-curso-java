package app.services;

import java.util.ArrayList;
import java.util.List;

import app.beans.Estudiante;
import app.repositories.EstudianteRepository;

public class EstudianteServicesImpl implements EstudianteServices {

    private EstudianteRepository estudianteRepositorio = EstudianteRepository.newInstance("C");
    
    public void selectEstudiantes() throws Exception{
        List<Estudiante> estudiantes = estudianteRepositorio.selectAll();
        estudiantes.forEach(estudiante -> {
            System.out.println(estudiante.toString());
        });
    }

    public void insertEstudiante() throws Exception {
        Estudiante nuevoEstudiante = new Estudiante()
            .withNombre("Belle")
            .withApellido("Rhodes")
            .withEdad(25)
            .withEscuela("Escuela plain");
        Integer nuevo = estudianteRepositorio.insert(nuevoEstudiante);
        System.out.println("Registros insertados: " + nuevo);
    }

    public void insertEstudiantes() throws Exception {
        List<Estudiante> estudiantes = new ArrayList<Estudiante>();

        Estudiante estudianteUno = new Estudiante();
        estudianteUno.setNombre("Eddie");
        estudianteUno.setApellido("Gray");
        estudianteUno.setEdad(20);
        estudianteUno.setEscuela("Escuela loud");

        Estudiante estudianteDos = new Estudiante();
        estudianteDos.setNombre("Harriett");
        estudianteDos.setApellido("Patrick");
        estudianteDos.setEdad(24);
        estudianteDos.setEscuela("Escuela faster");

        estudiantes.add(estudianteUno);
        estudiantes.add(estudianteDos);
        
        Integer nuevos = estudianteRepositorio.insertAll(estudiantes);
        System.out.println("Registros insertados: " + nuevos);
    }

    public void updateEstudiante() throws Exception {
        Estudiante estudiante = new Estudiante();
        estudiante.setId(10L);
        estudiante.setNombre("Derrick");
        estudiante.setApellido("Clayton");
        estudiante.setEdad(25);
        estudiante.setEscuela("Escuela listen");
        Integer actualizado = estudianteRepositorio.update(estudiante);
        System.out.println("Registros actualizados: " + actualizado);
    }

    public void updateEstudiantes() throws Exception {
        List<Estudiante> estudiantes = new ArrayList<Estudiante>();

        Estudiante estudianteUno = new Estudiante();
        estudianteUno.setId(17L);
        estudianteUno.setNombre("Sophia");
        estudianteUno.setApellido("Robinson");
        estudianteUno.setEdad(19);
        estudianteUno.setEscuela("Escuela grain");

        Estudiante estudianteDos = new Estudiante();
        estudianteDos.setId(1L);
        estudianteDos.setNombre("Sophie");
        estudianteDos.setApellido("Peters");
        estudianteDos.setEdad(20);
        estudianteDos.setEscuela("Escuela solar");

        estudiantes.add(estudianteUno);
        estudiantes.add(estudianteDos);
        
        Integer actualizados = estudianteRepositorio.updateMany(estudiantes);
        System.out.println("Registros actualizados: " + actualizados);
    }

    public void deleteEstudiante() throws Exception {
        Estudiante estudiante = new Estudiante();
        estudiante.setId(5L);
        Integer eliminado = estudianteRepositorio.delete(estudiante.getId());
        System.out.println("Registros eliminados: " + eliminado);
    }

    public void deleteEstudiantes() throws Exception {
        List<Long> idsEstudiantes = new ArrayList<Long>();
        idsEstudiantes.add(1L);
        idsEstudiantes.add(2L);
        idsEstudiantes.add(3L);
        Integer eliminado = estudianteRepositorio.deleteMany(idsEstudiantes);
        System.out.println("Registros eliminados: " + eliminado);
    }
}