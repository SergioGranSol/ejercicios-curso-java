package app.services;

public interface EstudianteServices {
    void selectEstudiantes() throws Exception;
    void insertEstudiante() throws Exception;
    void insertEstudiantes() throws Exception;
    void updateEstudiante() throws Exception;
    void updateEstudiantes() throws Exception;
    void deleteEstudiante() throws Exception;
    void deleteEstudiantes() throws Exception;
}