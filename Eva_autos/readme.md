# Cambiar el puerto de deployment del war al desplegar en JBoss

**NOTA:** Cambiar [el-nombre-de-mi-host] en los fragmentos de los xml
_Ejemplo_ -> [el-nombre-de-mi-host] cambiar por: mi-deployment

En el archivo standalone.xml de JBoss agregar a **<socket-binding-group**

    <socket-binding name="[el-nombre-de-mi-host]-manager" port="8081"/>

En el archivo standalone.xml de JBoss agregar a **<subsystem xmlns="urn:jboss:domain:undertow**

    <server name="[el-nombre-de-mi-host]-server">
        <http-listener name="[el-nombre-de-mi-host]-listener" socket-binding="[el-nombre-de-mi-host]-manager"/>
        <host name="[el-nombre-de-mi-host]" alias="localhost,127.0.0.1"/>
    </server>

Crear en **/WEB-INF/** del proyecto el archivo **jboss-web.xml** y agregar

    <?xml version="1.0" encoding="UTF-8"?>
    <jboss-web>
        <virtual-host>[el-nombre-de-mi-host]</virtual-host>
        <server-instance>[el-nombre-de-mi-host]-server</server-instance>
        <context-root>/</context-root>
    </jboss-web>

**_Al desplegar se podra ingresar al deploy mediante la ruta: http://localhost:8081/_**