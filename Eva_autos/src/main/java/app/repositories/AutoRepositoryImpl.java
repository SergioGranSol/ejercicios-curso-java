package app.repositories;

import java.util.ArrayList;
import java.util.List;

import app.beans.Auto;

public class AutoRepositoryImpl implements AutoRepository {

    private List<Auto> autos = new ArrayList<Auto>();
    private Long newId = 6L;
    public AutoRepositoryImpl(){
        autos.add(new Auto(1L, "Forte", "Kia", 2020, "Azul"));
        autos.add(new Auto(2L, "Rio", "Kia", 2020, "Rojo"));
        autos.add(new Auto(3L, "Versa", "Nissan", 2020, "Azul"));
        autos.add(new Auto(4L, "Civic", "Honda", 2019, "Negro"));
        autos.add(new Auto(5L, "City", "Honda", 2019, "Plata"));
    }

    @Override
    public List<Auto> findAll() {
        return autos;
    }

    @Override
    public Long insert(Auto autoNuevo) {
        autoNuevo.setId(this.newId++);
        autos.add(autoNuevo);
        return autoNuevo.getId();
    }

    @Override
    public Long delete(Long id) {
        for(int i = 0; i < autos.size(); i++){
            if(autos.get(i).getId().equals(id)){
                autos.remove(i);
                break;
            }
        }
        return id;
    }

}