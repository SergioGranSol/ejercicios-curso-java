package app.repositories;

import java.util.List;

import app.beans.Auto;


public interface AutoRepository {
    List<Auto> findAll();
    Long insert(Auto autoNuevo);
    Long delete(Long id);

    static AutoRepository newInstance(Boolean type){
        return new AutoRepositoryImpl();
    }
}