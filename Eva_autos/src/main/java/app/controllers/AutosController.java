package app.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.beans.Auto;
import app.services.AutoService;

public class AutosController extends HttpServlet {

    private static final long serialVersionUID = 5620008826112544498L;
    private static final AutoService servicio = new AutoService();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List <Auto> autos = servicio.getAutos();
        request.setAttribute("autos", autos);
        getServletContext().getRequestDispatcher("/WEB-INF/views/autosList.jsp").forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("accion");
        if("add".equals(action)){
            Auto autoNuevo = new Auto();
            autoNuevo
                .withNombre(Optional.ofNullable(request.getParameter("nombre")).orElse("Auto sin nombre"))
                .withMarca(Optional.ofNullable(request.getParameter("marca")).orElse("Auto sin marca"))
                .withModelo(Integer.parseInt(Optional.ofNullable(request.getParameter("modelo")).orElse("2000")))
                .withColor(Optional.ofNullable(request.getParameter("color")).orElse("Gris"));
            servicio.addAuto(autoNuevo);
        }
        if("remove".equals(action)){
            servicio.removeAuto(Long.parseLong(Optional.ofNullable(request.getParameter("autoId")).orElse("0")));
        }
        response.sendRedirect("/autos");
        // List <Auto> autos = servicio.getAutos();
        // request.setAttribute("autos", autos);
        // getServletContext().getRequestDispatcher("/WEB-INF/views/autosList.jsp").forward(request, response);
    }
}