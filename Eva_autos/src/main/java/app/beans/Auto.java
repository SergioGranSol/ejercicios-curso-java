package app.beans;

public class Auto {
    private Long id;
    private String nombre;
    private String marca;
    private Integer modelo;
    private String color;

    public Auto(){ }

    public Auto(Long id, String nombre, String marca, Integer modelo, String color) {
        this.id = id;
        this.nombre = nombre;
        this.marca = marca;
        this.modelo = modelo;
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getModelo() {
        return modelo;
    }

    public void setModelo(Integer modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Auto withId(Long id){
        this.id = id;
        return this;
    }

    public Auto withNombre(String nombre){
        this.nombre = nombre;
        return this;
    }

    public Auto withMarca(String marca){
        this.marca = marca;
        return this;
    }

    public Auto withModelo(Integer modelo){
        this.modelo = modelo;
        return this;
    }

    public Auto withColor(String color){
        this.color = color;
        return this;
    }

    @Override
    public String toString() {
        return "Auto [modelo=" + modelo + ", marca=" + marca + ", nombre="
                + nombre + ", color=" + color + "]";
    }
}