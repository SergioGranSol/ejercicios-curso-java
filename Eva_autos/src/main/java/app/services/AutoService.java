package app.services;

import java.util.List;

import app.beans.Auto;
import app.repositories.AutoRepository;

public class AutoService {

    private AutoRepository AutoRepositorio = AutoRepository.newInstance(true);

    public List<Auto> getAutos() {
        List<Auto> autos = AutoRepositorio.findAll();
        autos.forEach(auto -> {
            System.out.println(auto);
        });
        return autos;
    }

    public Long addAuto(Auto autoNuevo){
        return AutoRepositorio.insert(autoNuevo);
    }

    public Long removeAuto(Long id){
        return AutoRepositorio.delete(id);
    }
}