<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<jsp:include page="./header.jsp" flush="true">
    <jsp:param name="title" value="Lista de autos"/>
</jsp:include>

<body>
    <main role="main">
        <section class="jumbotron text-center">
            <div class="container">
                <h1>Catalogo de autos</h1>
                <p class="lead text-muted">Agrega un auto usando el formulario</p>
                <hr>
                <form method="POST" action="/autos/create" target="_self">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nombre" name="nombre" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Marca</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="marca" name="marca" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Modelo</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="modelo" name="modelo" required="" min="1990" max="2025">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Color</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="color" name="color" required="">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" name="accion" value="add">Guardar</button>
                </form>
                <hr>
                <form method="POST" action="/autos/delete" target="_self">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-danger" name="accion" value="remove">Eliminar</button>
                        </div>
                        <input type="number" class="form-control" name="autoId" placeholder="Id del auto" min="1">
                    </div>
                </form>
            </div>
        </section>
        <div class="container">
            <table class="table table-bordered table-sm">
                <thead class="thead-dark"><tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Modelo</th>
                    <th scope="col">Color</th>
                </tr></thead>
                <tbody>
                    <c:forEach items="${autos}" var="auto"><tr>
                        <td>${auto.id}</td>
                        <td>${auto.nombre}</td>
                        <td>${auto.marca}</td>
                        <td>${auto.modelo}</td>
                        <td>${auto.color}</td>
                    </tr></c:forEach>
                </tbody>
            </table>
        </div>
    </main>

</body>
</html>