1. que ventajas y desventajas de un jsp
    ven:    Se tiene logica en la vista que se sirve a cliente
            Se pueden manejar variables de java para mostrar del lado del cliente
    des:    Con malas practicas se pondira demasiada logica de negocio aqui

2. la diferencia entre las cookies y el httpsession
    cookies es la informacion que se almacena en la maquina cliente
    httpsession es la sesion con la informacion que existe durante la comunicacion de cliente servidor

3. como se utiliza la inclusión dinámica y estática?

4. dado el siguiente fragmento de web.xml
    <servlet-mapping>
        <servlet-name>ServletA</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
    <servlet-mapping>
        <servlet-name>ServletB</servlet-name>
        <url-pattern>/bservlet.html</url-pattern>
    </servlet-mapping>
    <servlet-mapping>
        <servlet-name>ServletC</servlet-name>
        <url-pattern>*.servletC</url-pattern>
    </servlet-mapping>
    <servlet-mapping>
        <servlet-name>ServletD</servlet-name>
        <url-pattern>/dservlet/*</url-pattern>
    </servlet-mapping>
y si un usuario ingresa la siguiente url http://myserver:8080/mywebapp/Bservlet.html que servlet será ejecutado?
    el segundo servlet ya que la ruta /Bservlet.html esta mapeada al serlvet ServletB

5. qué son los tipos mime y para qué se utilizan?
    son los tipos de archivos de respuesta de una peticion http