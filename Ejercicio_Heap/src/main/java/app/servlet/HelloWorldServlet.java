package app.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet {
    private static final long serialVersionUID = 2776005131823298964L;
    private static int counter = 0;
    @Override
    public void doGet(HttpServletRequest r, HttpServletResponse q) {
        q.setContentType("text/html;charset=UTF-8");
        System.out.println("Receive request " + counter ++);
        try (PrintWriter pw = q.getWriter()) {
            pw.println("<html><head><title>Hello World</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" /></head>");
            pw.println("<body><h2>Hola Mundo con mi Servlet</h2></body></html>");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}