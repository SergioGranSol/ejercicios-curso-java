package app;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        try {
            System.out.println("Iniciando Thread");
            Thread.sleep(10000);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        ArrayList<String> arrayStrings = new ArrayList<String>();

        for (int j = 0; j < 100000000; j++) {
            for (int i = 0; i < 1000000; i++) {
                try {
                    Thread.sleep(1);
                    String s = i + j + "Hola a todos ";
                    arrayStrings.add(s);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
