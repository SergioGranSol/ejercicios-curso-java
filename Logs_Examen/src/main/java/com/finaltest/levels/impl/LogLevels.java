package com.finaltest.levels.impl;

import com.finaltest.levels.LogLevelable;

import org.apache.logging.log4j.Level;

public class LogLevels implements LogLevelable {
    public final static Level OFF = Level.forName("OFF", 0);
    public final static Level MUSTFIX = Level.forName("MUSTFIX", 1);
    public final static Level DATABASE = Level.forName("DATABASE", 250);
    public final static Level FAILOVER = Level.forName("FAILOVER", 350);
    public final static Level FIXLATER = Level.forName("FIXLATER", 450);
    public final static Level MYDEBUG = Level.forName("MYDEBUG", 550);
    public final static Level ALL = Level.forName("ALL", Integer.MAX_VALUE);

    @Override
    public void createLevels() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dummy1() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dummy2() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dummy3() {
        // TODO Auto-generated method stub

    }
}