package com.finaltest.thread;

public class Threads {

    private Thread0 t0 = new Thread0();
    private Thread1 t1 = new Thread1();
    private Thread2 t2 = new Thread2();
    private Thread3 t3 = new Thread3();
    private Thread4 t4 = new Thread4();
    private Thread5 t5 = new Thread5();
    private Thread6 t6 = new Thread6();
    private Thread7 t7 = new Thread7();
    private Thread8 t8 = new Thread8();
    private Thread9 t9 = new Thread9();
    private Thread10 t10 = new Thread10();
    private Thread11 t11 = new Thread11();
    private Thread12 t12 = new Thread12();

    public void startThreads(){
        t0.startThread();
        t1.startThread();
        t2.startThread();
        t3.startThread();
        t4.startThread();
        t5.startThread();
        t6.startThread();
        t7.startThread();
        t8.startThread();
        t9.startThread();
        t10.startThread();
        t11.startThread();
        t12.startThread();
    }
}