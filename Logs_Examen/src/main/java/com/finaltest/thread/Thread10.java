package com.finaltest.thread;

import com.finaltest.levels.impl.LogLevels;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Thread10 {

    private static final Logger logger = LogManager.getLogger();

    public void startThread(){
        Thread t = new Thread(()-> {
            ThreadContext.push("name", "Sergio Granados");
			ThreadContext.push("userName", "Jessica Drew");
            logger.log(LogLevels.MYDEBUG, "Test MYDEBUG");
        }, "Thread 10");
        t.start();
    }

}