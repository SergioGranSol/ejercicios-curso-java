package com.finaltest.thread;

import com.finaltest.levels.impl.LogLevels;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Thread1 {

    private static final Logger logger = LogManager.getLogger();

    public void startThread(){
        Thread t = new Thread(()-> {
            ThreadContext.push("name", "Sergio Granados");
			ThreadContext.push("userName", "Peter Parker");
            logger.log(LogLevels.MUSTFIX, "Test MUSTFIX");
        }, "Thread 1");
        t.start();
    }

}