package com.finaltest.thread;

import com.finaltest.levels.impl.LogLevels;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Thread8 {

    private static final Logger logger = LogManager.getLogger();

    public void startThread(){
        Thread t = new Thread(()-> {
            ThreadContext.push("name", "Sergio Granados");
			ThreadContext.push("userName", "Carol Danvers");
            logger.log(LogLevels.FIXLATER, "Test FIXLATER");
        }, "Thread 8");
        t.start();
    }

}