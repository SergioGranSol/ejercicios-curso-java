package com.finaltest.thread;

import com.finaltest.levels.impl.LogLevels;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class Thread12 {

    private static final Logger logger = LogManager.getLogger();

    public void startThread(){
        Thread t = new Thread(()-> {
            ThreadContext.push("name", "Sergio Granados");
			ThreadContext.push("userName", "Jean Grey");
            logger.log(LogLevels.MYDEBUG, "Test ALL");
        }, "Thread 12");
        t.start();
    }

}