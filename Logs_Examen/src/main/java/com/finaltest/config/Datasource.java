package com.finaltest.config;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;

public class Datasource {

    private static BasicDataSource dataSource = null;
    private static String url = "jdbc:postgresql://localhost:5432/cursoJava?user=postgres&password=postgres";

    public Datasource() { }

    public static Connection crearConexion() throws SQLException {
        if (dataSource == null){
            dataSource = new BasicDataSource();
            dataSource.setUrl(url);
        }
        return dataSource.getConnection();  
    }

}