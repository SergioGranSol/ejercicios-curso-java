package app;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {
        List<Alumno> alumnos = new ArrayList<Alumno>();
        alumnos.add(new Alumno("Rosetta", "Morgan", "Perez", "Curso de Java"));
        alumnos.add(new Alumno("Adele", "Cook", "Franklin", "Curso de Java"));
        alumnos.add(new Alumno("Eva", "Newton", "Tyler", "Curso de Java"));
        alumnos.add(new Alumno("Lois", "Moreno", "George", "Curso de Java"));
        alumnos.add(new Alumno("Rachel", "Cooper", "Holmes", "Curso de Java"));
        alumnos.add(new Alumno("Charlie", "Conner", "Davidson", "Curso de Java"));
        alumnos.add(new Alumno("Darrell", "Rose", "Holloway", "Curso de Java"));
        alumnos.add(new Alumno("Luis", "Collier", "Rodgers", "Curso de Java"));
        alumnos.add(new Alumno("Lucille", "West", "Mills", "Curso de Java"));
        alumnos.add(new Alumno("Dennis", "Freeman", "McKenzie", "Curso de Java"));
        alumnos.add(new Alumno("Aaron", "Byrd", "Moss", "Curso de Java"));
        alumnos.add(new Alumno("Clifford", "Bass", "Graves", "Curso de Java"));
        alumnos.add(new Alumno("Philip", "Barrett", "Bailey", "Curso de Java"));
        alumnos.add(new Alumno("Paul", "Moss", "Brooks", "Curso de Java"));
        alumnos.add(new Alumno("Lester", "Washington", "McLaughlin", "Curso de Java"));
        alumnos.add(new Alumno("Mayme", "Richards", "Kim", "Curso de Java"));
        alumnos.add(new Alumno("Louis", "Tucker", "Mack", "Curso de Java"));
        alumnos.add(new Alumno("Nina", "Walters", "Taylor", "Curso de Java"));
        alumnos.add(new Alumno("Isaiah", "Singleton", "Summers", "Curso de Java"));
        alumnos.add(new Alumno("Mamie", "Jordan", "Brock", "Curso de Java"));

        System.out.println("Alumnos:");
        alumnos.stream().forEach(alumno -> System.out.println(alumno.toString()));
        
        System.out.println("\nLongitud de lista: " + alumnos.size());
        
        System.out.println("\nAlumnos con nombre que termina en 'a':");
        alumnos.stream()
            .filter(alumno -> alumno.getNombre().endsWith("a"))
            .forEach(alumno -> System.out.println(alumno.toString()));
        
        System.out.println("\nCinco primeros alumnos:");
        alumnos.stream().limit(5).forEach(alumno -> System.out.println(alumno.toString()));
    }
}

class Alumno {
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String nombreCurso;

    public Alumno(String nombre, String apellidoPaterno, String apellidoMaterno, String nombreCurso) {
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.nombreCurso = nombreCurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }

    @Override
    public String toString() {
        return "Alumno [apellidoMaterno=" + apellidoMaterno + ", apellidoPaterno=" + apellidoPaterno + ", nombre="
                + nombre + ", nombreCurso=" + nombreCurso + "]";
    }
}